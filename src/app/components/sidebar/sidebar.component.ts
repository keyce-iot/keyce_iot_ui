import {Component, OnInit} from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    {
        path: '/dashboard',
        title: 'Tableau de bord',
        icon: 'icon-chart',
        class: ''
    },
    {
        path: '/maps',
        title: 'Position du capteur',
        icon: 'icon-pin',
        class: ''
    },
    {
        path: '/configuration',
        title: 'Configuration des alertes',
        icon: 'icon-sliders',
        class: ''
    },
    {
        path: '/user',
        title: 'Mon compte',
        icon: 'icon-single',
        class: ''
    }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() {
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        return window.innerWidth <= 991;
    }
}

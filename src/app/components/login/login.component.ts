import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../_auth/auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private authService: AuthService,
                private formBuilder: FormBuilder
    ) {
    }

    public user = true;
    public form: FormGroup;
    public messageAlert = {
        actif: false,
        type: '',
        message: ''
    };

    ngOnInit() {
        this.form = new FormGroup({
            email: new FormControl('keyce@e-cdp.com', Validators.minLength(2)),
            password: new FormControl('xyeWJZmgdY')
        });
    }

    onSubmit(): void {
        this.authService.login(this.form.get('email').value, this.form.get('password').value).subscribe(data => {
            this.authService.isLogged.next(true);
        });
    }

    lostPassword(): void {
        this.authService.lostPassword(this.form.get('email').value).subscribe(data => {
            this.messageAlert.actif = true;
            this.messageAlert.type = 'success';
            this.messageAlert.message = 'Un E-mail vient d\'être envoyé à l\'adresse : ' + this.form.get('email').value;
            this.form.reset();
        });
    }

}

/* tslint:disable:quotemark */
import {Component, OnInit, ElementRef, OnDestroy, Input} from '@angular/core';
import {ROUTES} from '../sidebar/sidebar.component';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../_auth/auth.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

    constructor(
        location: Location,
        private element: ElementRef,
        public router: Router,
        private modalService: NgbModal,
        private authService: AuthService
    ) {
        this.location = location;
        this.sidebarVisible = false;
    }

    private listTitles: any[];
    private toggleButton: any;
    private sidebarVisible: boolean;
    public location: Location;
    public mobileMenuVisible: any = 0;
    public isCollapsed = true;
    @Input() logged;

    closeResult: string;

    private static getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    logout(): void {
        // TODO : a revoir
        this.authService.isLogged.next(false);
        this.authService.logout();
        this.authService.isLogged.subscribe(data => {
            this.logged = data;
        });
    }

    // functionthat adds color white/transparent to the navbar on resize (this is for the collapse)
    updateColor = () => {
        const navbar = document.getElementsByClassName('navbar')[0];
        if (window.innerWidth < 993 && !this.isCollapsed) {
            navbar.classList.add('bg-white');
            navbar.classList.remove('navbar-transparent');
        } else {
            navbar.classList.remove('bg-white');
            navbar.classList.add('navbar-transparent');
        }
    };

    ngOnInit() {
        this.authService.isLogged.subscribe(data => {
            this.logged = data;
        });
        window.addEventListener('resize', this.updateColor);
        this.listTitles = ROUTES.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.router.events.subscribe(event => {
            this.sidebarClose();
            const $layer: any = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
                this.mobileMenuVisible = 0;
            }
        });
    }

    collapse() {
        this.isCollapsed = !this.isCollapsed;
        const navbar = document.getElementsByTagName('nav')[0];
        if (!this.isCollapsed) {
            navbar.classList.remove('navbar-transparent');
            navbar.classList.add('bg-white');
        } else {
            navbar.classList.add('navbar-transparent');
            navbar.classList.remove('bg-white');
        }
    }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const mainPanel = (
            document.getElementsByClassName('main-panel')[0]
        ) as HTMLElement;
        const html = document.getElementsByTagName('html')[0];
        if (window.innerWidth < 991) {
            mainPanel.style.position = 'fixed';
        }

        // tslint:disable-next-line:only-arrow-functions
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);

        html.classList.add('nav-open');

        this.sidebarVisible = true;
    }

    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        this.toggleButton.classList.remove('toggled');
        const mainPanel = (
            document.getElementsByClassName('main-panel')[0]
        ) as HTMLElement;

        if (window.innerWidth < 991) {
            // tslint:disable-next-line:only-arrow-functions
            setTimeout(function () {
                mainPanel.style.position = '';
            }, 500);
        }
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    }

    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        const $layer = document.createElement('div');
// const html = document.getElementsByTagName('html')[0];
        const $toggle = document.getElementsByClassName('navbar-toggler')[0];

        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const html = document.getElementsByTagName('html')[0];

        if (this.mobileMenuVisible === 1) {
            // $('html').removeClass('nav-open');
            html.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }
            // tslint:disable-next-line:only-arrow-functions
            setTimeout(function () {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobileMenuVisible = 0;
        } else {
            // tslint:disable-next-line:only-arrow-functions
            setTimeout(function () {
                $toggle.classList.add('toggled');
            }, 430);

            $layer.setAttribute('class', 'close-layer');

            if (html.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (html.classList.contains('off-canvas-sidebar')) {
                document
                    .getElementsByClassName('wrapper-full-page')[0]
                    .appendChild($layer);
            }

            // tslint:disable-next-line:only-arrow-functions
            setTimeout(function () {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function () {
                // asign a function
                html.classList.remove('nav-open');
                this.mobileMenuVisible = 0;
                $layer.classList.remove('visible');
                // tslint:disable-next-line:only-arrow-functions
                setTimeout(function () {
                    $layer.remove();
                    $toggle.classList.remove('toggled');
                }, 400);
            }.bind(this);

            html.classList.add('nav-open');
            this.mobileMenuVisible = 1;
        }
    }

    getTitle() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }

        // tslint:disable-next-line:prefer-for-of
        for (let item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Dashboard';
    }

    open(content) {
        this.modalService.open(content, {windowClass: 'modal-search'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${NavbarComponent.getDismissReason(reason)}`;
        });
    }

    ngOnDestroy() {
        window.removeEventListener('resize', this.updateColor);
    }

    redirect(route): void {
        if (route === 'profile') {
            this.router.navigate(['/user']);
        }
    }
}

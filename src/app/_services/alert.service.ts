import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ApiGlobals} from '../../const-global/global';

const httpOptions = {
    headers: new HttpHeaders(
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    )
};

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    public dataAlertSubject = new Subject<any>();

    constructor(private http: HttpClient) {
    }

    getDatasAlert(): Observable<any> {
        return this.http.get(ApiGlobals.API_URL + 'datas/alert');
    }

    createAlert(form: any): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'datas/alert', {
            alert: form,
        }, httpOptions);
    }

    deleteAlert(id: number): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'datas/alert/' + id, {
            alertId: id,
        }, httpOptions);
    }

    editAlert(form: any): Observable<any> {
        return this.http.put(ApiGlobals.API_URL + 'datas/alert', {
            alert: form,
        }, httpOptions);
    }
}

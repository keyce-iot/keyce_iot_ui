import { TestBed } from '@angular/core/testing';

import { CaptorService } from './captor.service';

describe('CaptorService', () => {
  let service: CaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

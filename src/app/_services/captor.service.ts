import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, interval, Observable, Subject, Subscription} from 'rxjs';
import {ApiGlobals} from '../../const-global/global';

// const API_URL = environment.apiURL;
const httpOptions = {
    headers: new HttpHeaders(
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            accept: 'application/son'
        }
    )
};

@Injectable({
    providedIn: 'root'
})
export class CaptorService {

    private apiSubscription: Subscription;

    public dataCaptorSubject = new Subject<any>();

    constructor(private http: HttpClient) {
        // Empêche l'effet blink on appel une première fois le compteur
        const firstCall = this.getDatasCaptor();
        const subs = firstCall.subscribe(value => {
            this.dataCaptorSubject.next(value);
        });
        // récupère les nouvelles valeurs toutes les n secondes
        this.apiSubscription = interval(ApiGlobals.POLLING_INTERVAL).subscribe(() => {
            subs.unsubscribe();
            const obs = this.getDatasCaptor();
            obs.subscribe(value => {
                this.dataCaptorSubject.next(value);
            });
        });
    }

    getDatasCaptor(): Observable<any> {
        return this.http.get(ApiGlobals.API_URL + 'datas/capteur');
    }

    sendAlertByMail(datas): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'mail', {
            alert: datas
        }, httpOptions);
    }
}

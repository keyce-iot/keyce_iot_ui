import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {UserComponent} from './pages/user/user.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuardService as AuthGuard} from './_auth/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
            },
            {
                path: 'map',
                redirectTo: 'map',
            },
            {
                path: 'configuration',
                redirectTo: 'configuration',
            },
            {
                path: 'user',
                redirectTo: 'user',
            },
            {
                path: '',
                loadChildren: () => import ('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
            },
            {
                path: 'user',
                component: UserComponent,
            }
        ]
    },
    // Routes sans layout
    {path: 'login', component: LoginComponent}
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {
            useHash: true
        })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map, Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ApiGlobals} from '../../const-global/global';

const USER_KEY = 'jwt';
const httpOptions = {
    headers: new HttpHeaders(
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            accept: 'application/son'
        }
    )
};

const jwtHelper = new JwtHelperService();

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public isLogged = new Subject<any>();

    constructor(
        private http: HttpClient,
        private router: Router,
    ) {
    }

    public isAuthenticated(): boolean {
        const token = localStorage.getItem('jwt');
        return !jwtHelper.isTokenExpired(token);
    }

    public login(email: string, password: string): Observable<void> {
        return this.http.post(ApiGlobals.API_URL + 'login_check', {email, password}, httpOptions)
            .pipe(
                map(response => {
                    // login successful if there's a jwt token in the response
                    if (response) {
                        localStorage.setItem('jwt', JSON.stringify(response));
                        this.router.navigate(['/dashboard']);
                    }
                })
            );
    }

    public lostPassword(email: string): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'login/lost-password', {
            email
        }, httpOptions);
    }

    public logout(): void {
        window.localStorage.clear();
        window.sessionStorage.clear();
        window.localStorage.removeItem(USER_KEY);
        this.router.navigate(['login']);
    }

    public register(username: string, email: string, password: string): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'signup', {
            email,
            password
        }, httpOptions);
    }

    public sendMotDePasse(login: string): Observable<any> {
        return this.http.post(ApiGlobals.API_URL + 'send-password', {
            login
        }, httpOptions);
    }
}

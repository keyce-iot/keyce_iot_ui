import {TestBed} from '@angular/core/testing';

import {AuthGuardService} from './auth-guard.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('AuthGuardService', () => {
    let guard: AuthGuardService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ]
        });
        guard = TestBed.inject(AuthGuardService);
    });

    it('should be created', () => {
        expect(guard).toBeTruthy();
    });
});

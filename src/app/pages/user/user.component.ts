import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../_auth/auth.service';
import {UserService} from '../../_services/user.service';

@Component({
    selector: 'app-user',
    templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {

    constructor(private authService: AuthService,
                private userService: UserService,
                private formBuilder: FormBuilder
    ) {
    }

    public user;
    public form: FormGroup;

    ngOnInit() {
        this.userService.getCurrentUser().subscribe(user => {
            this.user = user
            this.form = this.formBuilder.group({
                nom: [''],
                prenom: [''],
                roles: [''],
                email: [''],
                username: [''],
            });
            this.form.get('nom').setValue('John');
            this.form.get('prenom').setValue('Smith');
            this.form.get('email').setValue(this.user.email);
            this.form.get('roles').setValue(this.user.roles);
            this.form.get('username').setValue(this.user.username);
        });
    }
}

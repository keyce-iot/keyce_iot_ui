import {Component, OnInit} from '@angular/core';
import Chart from 'chart.js';
import {CaptorService} from '../../_services/captor.service';
import {AlertService} from '../../_services/alert.service';
import {Alert} from '../../../models/alert';
import * as moment from 'moment';
import 'moment/locale/fr'  // without this line it didn't work
moment.locale('fr')

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public datasets: any;
    public data: any;
    public myChartDataTemp;
    public myChartDataHum;
    public myChartDataBat;
    public datasCaptor = [];
    public clicked: boolean;
    public clicked1: boolean;
    public clicked2: boolean;
    public temp = []; // témperature
    public humidity = []; // humidité
    public battery = []; // batterie
    public datalabel = [];
    public messageAlertTemp = {
        actif: false,
        capteur: 'temperature',
        type: 'danger',
        message: ''
    };
    public messageAlertHum = {
        actif: false,
        capteur: 'humidite',
        type: 'danger',
        message: ''
    };

    constructor(private captorService: CaptorService, private alerteService: AlertService) {
    }

    ngOnInit() {
        // Récupération des data dans des tableau (un par graph)
        this.captorService.dataCaptorSubject.subscribe(datas => {
            this.datasCaptor = datas;
            this.datalabel = ['JAN', 'FEB', 'MAR', 'AVR', 'MAI', 'JUIN', 'JUL', 'AOUT', 'SEP', 'OCT', 'NOV', 'DEC'];
            const alerte = false;
            this.sendAlerts(alerte, this.temp, this.humidity);
            this.datasCaptor.forEach(values => {
                if (this.temp.length <= 100) {
                    this.temp.push(values.temperature);
                }
                if (this.humidity.length <= 100) {
                    this.humidity.push(values.humidity);
                }
                if (this.battery.length <= 100) {
                    this.battery.push(values.batLevel);
                }
            })

            // Définition des propriétés pour le graph de la température
            const gradientChartOptionsTemperature: any = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },

                tooltips: {
                    backgroundColor: '#f5f5f5',
                    titleFontColor: '#333',
                    bodyFontColor: '#666',
                    bodySpacing: 4,
                    xPadding: 12,
                    mode: 'nearest',
                    intersect: 0,
                    position: 'nearest'
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        barPercentage: 1.6,
                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(29,140,248,0.0)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            suggestedMin: 15,
                            suggestedMax: 30,
                            padding: 20,
                            fontColor: '#9a9a9a'
                        }
                    }],

                    xAxes: [{
                        barPercentage: 1.6,
                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(233,32,16,0.1)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            padding: 20,
                            fontColor: '#9a9a9a'
                        }
                    }]
                }
            };
            const canvasTemp = document.getElementById('chartTemp') as HTMLCanvasElement;
            const ctxTemp = canvasTemp.getContext('2d');
            const gradientStrokeTemp = ctxTemp.createLinearGradient(0, 230, 0, 50);
            gradientStrokeTemp.addColorStop(1, 'rgba(233,32,16,0.2)');
            gradientStrokeTemp.addColorStop(0.4, 'rgba(233,32,16,0.0)');
            gradientStrokeTemp.addColorStop(0, 'rgba(233,32,16,0)'); // red colors
            this.myChartDataTemp = new Chart(ctxTemp, {
                type: 'line',
                data: {
                    // labels: chartLabels,
                    labels: this.datalabel,
                    datasets: [{
                        label: 'Température',
                        fill: true,
                        backgroundColor: gradientStrokeTemp,
                        borderColor: '#ec250d',
                        borderWidth: 2,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        pointBackgroundColor: '#ec250d',
                        pointBorderColor: 'rgba(255,255,255,0)',
                        pointHoverBackgroundColor: '#ec250d',
                        pointBorderWidth: 20,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 15,
                        pointRadius: 4,
                        data: this.temp,
                    }]
                },
                options: gradientChartOptionsTemperature
            });

            // Config graph humidité
            const gradientChartOptionsHumidity: any = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },

                tooltips: {
                    backgroundColor: '#f5f5f5',
                    titleFontColor: '#333',
                    bodyFontColor: '#666',
                    bodySpacing: 4,
                    xPadding: 12,
                    mode: 'nearest',
                    intersect: 0,
                    position: 'nearest'
                },
                responsive: true,
                scales: {
                    yAxes: [{

                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(29,140,248,0.1)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 100,
                            padding: 20,
                            fontColor: '#9e9e9e'
                        }
                    }],

                    xAxes: [{

                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(29,140,248,0.1)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            padding: 20,
                            fontColor: '#9e9e9e'
                        }
                    }]
                }
            };
            const canvasHum = document.getElementById('chartHumidity') as HTMLCanvasElement;
            const ctxHum = canvasHum.getContext('2d');
            const gradientStrokeHum = ctxHum.createLinearGradient(0, 230, 0, 50);
            gradientStrokeHum.addColorStop(1, 'rgba(16,92,233,0.2)');
            gradientStrokeHum.addColorStop(0.4, 'rgba(16,77,233,0)');
            gradientStrokeHum.addColorStop(0, 'rgba(16,193,233,0)'); // red colors
            this.myChartDataHum = new Chart(ctxHum, {
                type: 'line',
                data: {
                    labels: this.datalabel,
                    datasets: [{
                        label: 'Humidité',
                        fill: true,
                        backgroundColor: gradientStrokeHum,
                        borderColor: '#0d7cec',
                        borderWidth: 2,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        pointBackgroundColor: '#0d7cec',
                        pointBorderColor: 'rgba(255,255,255,0)',
                        pointHoverBackgroundColor: '#0d7cec',
                        pointBorderWidth: 20,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 15,
                        pointRadius: 4,
                        data: this.humidity
                    }]
                },
                options: gradientChartOptionsHumidity
            });

            // Définition des propriétés pour le tableau de la batterie
            const gradientChartOptionsBattery: any = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },

                tooltips: {
                    backgroundColor: '#f5f5f5',
                    titleFontColor: '#333',
                    bodyFontColor: '#666',
                    bodySpacing: 4,
                    xPadding: 12,
                    mode: 'nearest',
                    intersect: 0,
                    position: 'nearest'
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        barPercentage: 1.6,
                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(29,140,248,0.0)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 100,
                            padding: 20,
                            fontColor: '#9e9e9e'
                        }
                    }],

                    xAxes: [{
                        barPercentage: 1.6,
                        gridLines: {
                            drawBorder: false,
                            color: 'rgba(0,242,195,0.1)',
                            zeroLineColor: 'transparent',
                        },
                        ticks: {
                            padding: 20,
                            fontColor: '#9e9e9e'
                        }
                    }]
                }
            };
            const canvasBat = document.getElementById('chartBattery') as HTMLCanvasElement;
            const ctxBat = canvasBat.getContext('2d');
            const gradientStrokeBat = ctxBat.createLinearGradient(0, 230, 0, 50);
            gradientStrokeBat.addColorStop(1, 'rgba(66,134,121,0.15)');
            gradientStrokeBat.addColorStop(0.4, 'rgba(66,134,121,0.0)'); // green colors
            gradientStrokeBat.addColorStop(0, 'rgba(66,134,121,0)');
            this.myChartDataBat = new Chart(ctxBat, {
                type: 'line',
                data: {
                    labels: this.datalabel,
                    datasets: [{
                        label: 'Batterie',
                        fill: true,
                        backgroundColor: gradientStrokeBat,
                        borderColor: '#00d6b4',
                        borderWidth: 2,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        pointBackgroundColor: '#00d6b4',
                        pointBorderColor: 'rgba(255,255,255,0)',
                        pointHoverBackgroundColor: '#00d6b4',
                        pointBorderWidth: 20,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 15,
                        pointRadius: 4,
                        data: this.battery
                    }]
                },
                options: gradientChartOptionsBattery
            });
        })
    }

    close(alert) {
        if (alert === this.messageAlertTemp.capteur) {
            this.messageAlertTemp.actif = false;
            this.messageAlertTemp.message = ''
        } else {
            this.messageAlertHum.actif = false;
            this.messageAlertHum.message = ''
        }
    }

    /**
     * regarde si un interval correspond afin d'envoyer une alerte par mail
     * si l'alerte est active et qu'elle est en dehors de l'interval
     * alors on envoie l'alerte
     */
    public sendAlerts(alerte, temp, humidity): void {
        this.alerteService.getDatasAlert().subscribe((datasAlerte: Array<Alert>) => {
            if (alerte === false) {
                temp.forEach(temperature => {
                    if (alerte === false) {
                        datasAlerte.forEach(item => {
                            if ((item.tempMin > temperature || item.tempMax < temperature) && alerte === false && item.active === true) {
                                alerte = true;
                                this.messageAlertTemp.actif = true;
                                this.messageAlertTemp.message = 'Une alerte de température vient de vous être envoyée par mail'
                                this.captorService.sendAlertByMail({
                                    name: item.name,
                                    temperatureCapteur: temperature,
                                    tempMin: item.tempMin,
                                    tempMax: item.tempMax,
                                }).subscribe();
                            }
                        });
                    }
                })
                alerte = false;
                humidity.forEach(hum => {
                    if (alerte === false) {
                        datasAlerte.forEach(item => {
                            if ((item.humMin > hum || item.humMax < hum) && alerte === false && item.active === true) {
                                alerte = true;
                                this.messageAlertHum.actif = true;
                                this.messageAlertHum.message = 'Une alerte d\'humidité vient de vous être envoyée par mail'
                                this.captorService.sendAlertByMail({
                                    name: item.name,
                                    humidityCapteur: hum,
                                    humMin: item.humMin,
                                    humMax: item.humMax,
                                }).subscribe();
                            }
                        });
                    }
                })
            }
        })
    }

    // Apres avoir cliquer sur Jour ==> on affiche les données par jour
    public getDatabyDay(): void {
        const dayLabel = [];
        this.temp = [];
        this.humidity = [];
        this.battery = [];
        this.datasCaptor.forEach(values => {
            if (dayLabel.length <= 23) {
                dayLabel.push(moment(values.dateTime).format('LT'))
            }

            if (this.temp.length <= 23) {
                this.temp.push(values.temperature);
            }

            if (this.humidity.length <= 23) {
                this.humidity.push(values.humidity);
            }

            if (this.battery.length <= 23) {
                this.battery.push(values.batLevel);
            }
        })

        this.myChartDataTemp.data.labels = dayLabel;
        this.myChartDataTemp.data.datasets.forEach((datasetTemp) => {
            datasetTemp.data.push(this.temp);
        });

        this.myChartDataBat.data.labels = dayLabel;
        this.myChartDataBat.data.datasets.forEach((datasetBat) => {
            datasetBat.data.push(this.battery);
        });

        this.myChartDataHum.data.labels = dayLabel;
        this.myChartDataHum.data.datasets.forEach((datasetHum) => {
            datasetHum.data.push(this.humidity);
        });

        this.myChartDataTemp.update();
        this.myChartDataBat.update();
        this.myChartDataHum.update();
    }

    // Apres avoir cliquer sur Week ==> on affiche les données par Semaine
    public getDatabyWeek(): void {
        const weekLabel = [];
        this.temp = [];
        this.humidity = [];
        this.battery = [];
        // const ttt = moment(values.dateTime).startOf('day').fromNow();

        this.datasCaptor.forEach(values => {
            if (weekLabel.length <= 7) {
                weekLabel.push(moment(values.dateTime).format('dddd'));
            }

            if (this.temp.length <= 7) {
                this.temp.push(values.temperature);
            }

            if (this.humidity.length <= 7) {
                this.humidity.push(values.humidity);
            }

            if (this.battery.length <= 7) {
                this.battery.push(values.batLevel);
            }
        })

        this.myChartDataTemp.data.labels = weekLabel;
        this.myChartDataTemp.data.datasets.forEach((datasetTemp) => {
            datasetTemp.data.push(this.temp);
        });

        this.myChartDataBat.data.labels = weekLabel;
        this.myChartDataBat.data.datasets.forEach((datasetBat) => {
            datasetBat.data.push(this.battery);
        });

        this.myChartDataHum.data.labels = weekLabel;
        this.myChartDataHum.data.datasets.forEach((datasetHum) => {
            datasetHum.data.push(this.humidity);
        });
    }

    // Apres avoir cliquer sur Mois ==> on affiche les données par mois
    public getDatabyMonth(): void {
        const monthLabel = [];
        this.temp = [];
        this.humidity = [];
        this.battery = [];
        this.datasCaptor.forEach(values => {
            if (monthLabel.length <= 31) {
                monthLabel.push(moment(values.dateTime).format('L'));
            }

            if (this.temp.length <= 31) {
                this.temp.push(values.temperature);
            }

            if (this.humidity.length <= 31) {
                this.humidity.push(values.humidity);
            }

            if (this.battery.length <= 31) {
                this.battery.push(values.batLevel);
            }
        })

        this.myChartDataTemp.data.labels = monthLabel;
        this.myChartDataTemp.data.datasets.forEach((datasetTemp) => {
            datasetTemp.data.push(this.temp);
        });

        this.myChartDataBat.data.labels = monthLabel;
        this.myChartDataBat.data.datasets.forEach((datasetBat) => {
            datasetBat.data.push(this.battery);
        });

        this.myChartDataHum.data.labels = monthLabel;
        this.myChartDataHum.data.datasets.forEach((datasetHum) => {
            datasetHum.data.push(this.humidity);
        });
    }
}

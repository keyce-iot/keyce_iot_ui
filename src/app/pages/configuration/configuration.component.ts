import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AlertService} from '../../_services/alert.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Alert} from '../../../models/alert';

@Component({
    selector: 'app-configuration',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit, AfterViewInit {

    public alerts: Array<Alert>;
    public alert: Alert;
    public form: FormGroup;
    public edit = false;
    closeResult = '';
    public messageAlert = {
        actif: false,
        type: '',
        message: ''
    };

    constructor(private alerteService: AlertService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder
    ) {
    }

    ngOnInit(): void {
        this.alerteService.getDatasAlert().subscribe(datas => {
            this.alerts = datas;
        });
        this.form = this.formBuilder.group({
            id: this.alert?.id,
            name: [this.alert?.name, Validators.required],
            tempMin: [this.alert?.tempMin, Validators.required],
            tempMax: [this.alert?.tempMax, Validators.required],
            humMin: [this.alert?.humMin, Validators.required],
            humMax: [this.alert?.humMax, Validators.required],
            actif: this.alert?.active,
        }, {updateOn: 'change'});
    }

    ngAfterViewInit() {
        this.alerteService.dataAlertSubject.subscribe(alert => {
            this.alerts = alert;
        })
    }

    onSubmit(type): void {
        const formData: any = {
            id: this.form.controls.id.value,
            name: this.form.controls.name.value,
            tempMin: this.form.controls.tempMin.value,
            tempMax: this.form.controls.tempMax.value,
            humMin: this.form.controls.humMin.value,
            humMax: this.form.controls.humMax.value,
            actif: this.form.controls.actif.value
        };

        if (type === 'create') {
            this.alerteService.createAlert(formData).subscribe(data => {
                this.alerteService.getDatasAlert().subscribe(dataUpdated => {
                    this.alerteService.dataAlertSubject.next(dataUpdated);
                    this.messageAlert.actif = true;
                    this.messageAlert.type = 'success';
                    this.messageAlert.message = 'L\'alerte ' + formData.name + ' vient d\être crée.'
                    this.form.reset();
                });
            });
        } else {
            this.alerteService.editAlert(formData).subscribe(() => {
                this.alerteService.getDatasAlert().subscribe(dataUpdated => {
                    this.alerteService.dataAlertSubject.next(dataUpdated);
                    this.messageAlert.actif = true;
                    this.messageAlert.type = 'warning';
                    this.messageAlert.message = 'L\'alerte ' + formData.name + ' vient d\être mise à jour.'
                    this.edit = false;
                    this.form.reset();
                });
            });
        }
    }

    desactiveAlert(id: number): void {
        this.alerteService.deleteAlert(id).subscribe((idAlert) => {
            this.alerts = this.alerts.filter((alert: Alert) => alert.id !== idAlert);
            this.messageAlert.actif = true;
            this.messageAlert.type = 'danger';
            this.messageAlert.message = 'L\'alerte ' + alert.name + ' vient d\être supprimée.'
        });
    }

    open(content, alert: Alert = null) {
        if (alert) {
            this.edit = true;
            this.form.get('id').setValue(alert.id);
            this.form.get('name').setValue(alert.name);
            this.form.get('tempMin').setValue(alert.tempMin);
            this.form.get('tempMax').setValue(alert.tempMax);
            this.form.get('humMin').setValue(alert.humMin);
            this.form.get('humMax').setValue(alert.humMax);
            this.form.get('actif').setValue(alert.active);
        }
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            this.edit = false;
        });
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    close() {
        this.messageAlert.actif = false;
        this.messageAlert.message = ''
    }

}

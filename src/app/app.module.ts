import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ToastrModule} from 'ngx-toastr';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './components/components.module';
import {JwtInterceptorService} from './_auth/jwt-interceptor.service';
import {AuthGuardService} from './_auth/auth-guard.service';
import {AuthService} from './_auth/auth.service';
import {BrowserModule} from '@angular/platform-browser';
import {RoleGuardService} from './_auth/role-guard.service';
import {JwtModule} from '@auth0/angular-jwt';

export function tokenGetter() {
    return window.localStorage.getItem('jwt');
}

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ComponentsModule,
        NgbModule,
        RouterModule,
        AppRoutingModule,
        ToastrModule.forRoot(),
        FontAwesomeModule,
        BrowserModule,
        JwtModule.forRoot({
            config: {
                tokenGetter,
                allowedDomains: ['http://localhost:4200'],
                disallowedRoutes: ['http://localhost:4200'],
            },
        }),
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent
    ],
    providers: [
        AuthService,
        AuthGuardService,
        RoleGuardService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptorService,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {DashboardComponent} from '../../pages/dashboard/dashboard.component';
import {MapComponent} from '../../pages/map/map.component';
import {NotificationsComponent} from '../../pages/notifications/notifications.component';
import {UserComponent} from '../../pages/user/user.component';
import {TablesComponent} from '../../pages/tables/tables.component';
import {TypographyComponent} from '../../pages/typography/typography.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationComponent} from '../../pages/configuration/configuration.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        HttpClientModule,
        NgbModule,
        ReactiveFormsModule,
    ],
    declarations: [
        DashboardComponent,
        UserComponent,
        TablesComponent,
        TypographyComponent,
        ConfigurationComponent,
        NotificationsComponent,
        MapComponent
    ]
})
export class AdminLayoutModule {
}

export class Alert {
    '@id'?: string;
    public id?: number;
    public name?: string;
    public tempMin?: any;
    public tempMax?: any;
    public humMin?: any;
    public humMax?: any;
    public active?: boolean;
    public dateCreation?: any;
    public dateModification?: any;
}

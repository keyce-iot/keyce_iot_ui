'use strict';

export class ApiGlobals {
    static readonly API_URL = 'http://localhost:8741/api/'
    static readonly POLLING_INTERVAL = (60000 * 60); // 15min
}

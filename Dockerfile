# base image
FROM node:12-alpine
RUN apk add --no-cache chromium
# set working directory
WORKDIR /app
# add `/front/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
# install and cache app dependencies
RUN npm install -g @angular/cli
# start app
EXPOSE 4200 49153
CMD npm install && ng serve --host 0.0.0.0  --poll 500 --disable-host-check --watch
